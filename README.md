## Test Cart&Order

### Demo https://focused-benz-cad057.netlify.app/

## Available Scripts

### `npm start`

Runs the app in the development mode.

### `npm run build`

Builds the app for production to the `build` folder.
