import React from 'react'
import { Avatar } from 'components/avatar'
import { StoreName } from 'components/store-name'
import { Notification } from 'components/notification'
import { Hamburger } from 'components/hamburger'
import './header.scss'

export function Header({ handleMenu, open }) {
	return (
		<header className='header-wrapper d-flex align-items-center justify-content-between'>
			<StoreName />
			<div className='d-flex align-items-center justify-content-between'>
				<Notification />
				<div className='divider' />
				<Avatar />
			</div>
			<div className='mob-menu'>
				<Hamburger handleMenu={handleMenu} open={open} />
			</div>
		</header>
	)
}
