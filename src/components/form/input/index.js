import React from 'react'
import InputMui from 'muicss/lib/react/input'

export function Input({ label, name, ...rest }) {
	return <InputMui name={name} label={label} {...rest} floatingLabel={true} />
}
