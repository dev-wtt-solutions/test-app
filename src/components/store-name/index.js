import React from 'react'
import './store-name.scss'

export function StoreName() {
	return (
		<div className='store-wrapper d-flex align-items-center'>
			<span className='d-flex align-items-center justify-content-center'>
				SN
			</span>
			<div className='name'>Store Name</div>
		</div>
	)
}
