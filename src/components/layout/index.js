import React, { useState } from 'react'
import Container from 'muicss/lib/react/container'
import Row from 'muicss/lib/react/row'
import { SideBar } from 'components/sidebar'
import { Header } from 'components/header'
import './layout.scss'

export default ({ children }) => {
	const [open, setOpen] = useState(false)
	const handleMenu = (data) => setOpen(data)
	return (
		<Container fluid={true}>
			<Row className='d-flex overflow-hidden'>
				<SideBar open={open} />
				<main className='main'>
					<Header handleMenu={handleMenu} open={open} />
					<div className='body'>{children}</div>
				</main>
			</Row>
		</Container>
	)
}
