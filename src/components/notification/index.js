import React from 'react'
import { Icon } from 'components/icons'

export function Notification() {
	return (
		<>
			<Icon component='NotificationIcon' />
		</>
	)
}
